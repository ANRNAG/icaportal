﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ICAPortal.Controllers
{
    public class ListController : Controller
    {
        // GET: List
        public ActionResult StudentsList()
        {
            return View();
        }
        public ActionResult UniversityList()
        {
            return View();
        }
        public ActionResult ProfessorsList()
        {
            return View();
        }
    }
}