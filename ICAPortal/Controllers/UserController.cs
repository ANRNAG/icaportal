﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace ICAPortal.Controllers
{
    public class UserController : Controller
    {
        // GET: User
        public ActionResult Login()
        {
            return View();
        }
       
        [HttpPost]
        public ActionResult Login(string uname, string pass)
        {
            if ((uname == "admin") && (pass == "admin"))
            {
                FormsAuthentication.SetAuthCookie(uname, true);
                Session["Login"] = "ADMIN";
                return RedirectToAction("Index", "Home");
            }
            return View();
        }

        public ActionResult Logout()
        {
            Session.RemoveAll();
            return RedirectToAction("Login", "User");
        }

    }
}