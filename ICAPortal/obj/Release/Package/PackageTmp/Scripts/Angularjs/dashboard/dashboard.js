﻿icamodule.controller("dashboard", ['$scope','$filter', '$http', 'webAPIurl', function ($scope, $filter, $http, webAPIurl) {

    $scope.waitdisplay = true;

    $scope.todate = new Date();
    $scope.fromdate = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate() - $scope.todate.getDate() + 1);
    //  
    $scope.model = {
        ToDateString: $filter('date')($scope.todate, "dd/MM/yyyy"),
        FromDateString: $filter('date')($scope.fromdate, "dd/MM/yyyy")
    }

 //   $scope.todate = new Date();
  //  $scope.fromdate = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate() - $scope.todate.getDate() + 1);
 //   $scope.frommaxDate =new Date($scope.todate);
//    $scope.tomaxDate = new Date();


    // alert('welcome to dashboard ICA')
    /*
  $scope.getstudentcounts = function()
  {
   public string FromDateString { get; set; }
            public string ToDateString { get; set; }
      alert('From date ' + $filter('date')($scope.fromdate, "dd/MM/yyyy") + 'To date ' + $filter('date')($scope.todate, "dd/MM/yyyy"));
  }
   $scope.model = {
            "FromDateString": $filter('date')($scope.fromdate, "dd/MM/yyyy"),
            "ToDateString": $filter('date')($scope.todate, "dd/MM/yyyy")
        }
   */

    $scope.getstudentcounts = function () {
        $scope.model = {
            ToDateString: $filter('date')($scope.todate, "dd/MM/yyyy"),
            FromDateString: $filter('date')($scope.fromdate, "dd/MM/yyyy")
        }
        $scope.processing = 'Processing....'
        //  alert('angular.toJson(model)');
        var studentscount = $http({
            method: "POST",
            url: webAPIurl + '/icaapi/GetDashBoardStudents',
            dataType: 'json',
            data: $scope.model,
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json, text/javascript"
            }
        });

        studentscount.then(function (resp, status) {
            console.log(resp.data.Response);
            $scope.students = resp.data.Response;
            $scope.processing = '';  // 
            //$scope.waitdisplay = true;
            
        }, function error(resp) {
            $scope.processing = resp.data.Message;
        });
        // window.location.href = '/Chat/History/senderid =' + model +', receiverid = '+model+', groupid ='+$scope.select_groupId;
    }
}]);