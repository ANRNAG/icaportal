﻿icamodule.controller("studentlist", ['ica_groups_service', '$scope', '$http', 'webAPIurl', '$interval', function (ica_groups_service, $scope, $http, webAPIurl, $interval) {

    //   $scope.orderByField = 'NoOfDays';
    //  $scope.reverseSort = true;

    //  $scope.Displaylist = 
    var startint = $interval(function () {
        $scope.waitdisplay = false;
        $scope.icagroups = ica_groups_service.geticagroupslookups();
        if ($scope.icagroups != '')
            $scope.stopint();
    }, 500);

    $scope.stopint = function () {
        //    if ($scope.userlookup != '' && $scope.gendelookup != '' && $scope.statuslookup != '')
        $interval.cancel(startint);
        $scope.waitdisplay = true;
        // console.log($scope.icagroups);
    }

    $scope.select_groupId = 0;

    $scope.get_studentlist = function () {
        //   alert($scope.select_groupId);
        $scope.processing = 'Processing....'
        $scope.Displaylist = false;
        $scope.studentslist = '';
        var geticagroups = $http({
            method: "get",
            url: webAPIurl + '/ICAApi/List/?StuGroupId=' + $scope.select_groupId,
            //    url: webAPIurl + '/Portal/GetPendingChat/' + $scope.select_groupId,
            dataType: 'json',
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json, text/javascript"
            }
        }).then(function (res, status) {
            //     alert('pending chat details ' + JSON.stringify(res.data.Response));
            if (res.data.Response == null || res.data.Response == undefined || res.data.Response == "" || res.data.Response.lenght == 0) {
                //   alert("No Data found");
                $scope.processing = 'No Data found'
                return
            }
            $scope.studentslist = res.data.Response;
            $scope.processing = '';
            //     console.log(res.data.Response);
        },
         function error(resp) {
             $scope.processing = resp.data.Message;
         });
    }

    $scope.getselectstudent = function (model) {
        model.GroupId = $scope.select_groupId;
        $scope.processing = 'Processing....'
        //  alert('angular.toJson(model)');
        var chat_history = $http({
            method: "POST",
            url: webAPIurl + '/StudentDetails',
            //    url: webAPIurl + '/Portal/GetPendingChat/' + $scope.select_groupId,
            dataType: 'json',
            data: model,
            //   data: model,
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json, text/javascript"
            }
        });

        chat_history.then(function (resp, status) {
        //    console.log(resp.data.Response);
            $scope.student = resp.data.Response;
            $scope.processing = '';  // 
            $scope.Displaylist = true;
            
        }, function error(resp) {
            $scope.processing = resp.data.Message;
        });
        // window.location.href = '/Chat/History/senderid =' + model +', receiverid = '+model+', groupid ='+$scope.select_groupId;
    }

    $scope.set_Displaylist = function () {
        $scope.Displaylist = !$scope.Displaylist;
    }


}]);