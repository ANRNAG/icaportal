﻿
icamodule.service('ica_groups_service', ['$http', 'webAPIurl', function ($http, webAPIurl) {
    var icagroups = '';

    var geticagroups = $http({
        method: "get",
        url: webAPIurl + '/ICAApi/Groups',
        //   url: 'http://localhost:49771/api/Lookups/getGenderLookup',  /Portal/GetPendingChat?id=
     //   url: webAPIurl + '/Portal/GetAllGroups',
        dataType: 'json',
        headers: {
            "Content-Type": "application/json",
            "Accept": "application/json, text/javascript"
        }
    }).then(function (res, status) {
        icagroups = res.data.Response;
    });

    this.geticagroupslookups = function (res, status) {
        return icagroups;
    }
   
}]);

icamodule.filter('ICA_NullDate', function () {
    return function (ICADate) {
        var ldate = new Date(ICADate);
        // var ddate = new Date("01/01/2000")
        if (ldate.getFullYear() == 2000)
            return '--';
        return ICADate;
    };
});