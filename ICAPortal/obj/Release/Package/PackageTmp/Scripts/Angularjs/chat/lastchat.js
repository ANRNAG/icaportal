﻿icamodule.controller("lastchat", ['ica_groups_service', '$scope', '$http', 'webAPIurl', '$interval', function (ica_groups_service, $scope, $http, webAPIurl, $interval) {

  //  $scope.orderByField = 'NoOfDays';
  //  $scope.reverseSort = true;

    var startint = $interval(function () {
        $scope.waitdisplay = false;
        $scope.icagroups = ica_groups_service.geticagroupslookups();
        if ($scope.icagroups != '')
            $scope.stopint();
    }, 500);

    $scope.stopint = function () {
        //    if ($scope.userlookup != '' && $scope.gendelookup != '' && $scope.statuslookup != '')
        $interval.cancel(startint);
        $scope.waitdisplay = true;
    //    console.log($scope.icagroups);
    }
    $scope.lchat_groupId = 0;

    $scope.get_lchat_details = function () {
        //   alert($scope.lchat_groupId);
        $scope.processing = 'Processing....'
        $scope.Displaychatlist = false;
        $scope.lchatmessages = '';
        var geticagroups = $http({
            method: "get",
            url: webAPIurl + '/ICAApi/Chat/?lastchatgroupid=' + $scope.lchat_groupId,
            //    url: webAPIurl + '/Portal/GetPendingChat/' + $scope.lchat_groupId,
            dataType: 'json',
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json, text/javascript"
            }
        }).then(function (res, status) {

            //     alert('pending chat details ' + JSON.stringify(res.data.Response));
            if (res.data.Response == null || res.data.Response == undefined || res.data.Response == "" || res.data.Response.lenght == 0) {
                //   alert("No Data found");
                $scope.processing = 'No Data found'
                return
            }
            $scope.lchatmessages = res.data.Response;
            $scope.processing = '';
            // console.log($scope.lchatmessages);
        },
         function error(resp) {
             $scope.processing = resp.data.Message;
         });
    }

    $scope.getselectchathistory = function (model) {

        model.GroupId = $scope.lchat_groupId;
        $scope.processing = 'Processing....'
        //  alert('angular.toJson(model)');
        var chat_history = $http({
            method: "POST",
            url: webAPIurl + '/ICAApi/Chat',
            //    url: webAPIurl + '/Portal/GetPendingChat/' + $scope.pchat_groupId,
            dataType: 'json',
            data: model,
            //   data: model,
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json, text/javascript"
            }
        });

        chat_history.then(function (resp, status) {
            console.log(resp.data.Response);
            $scope.detailchat = resp.data.Response;
            $scope.processing = '';  // 
            $scope.Displaychatlist = true;
            if ($scope.detailchat.StudentId != 0)
                $scope.is_Prof_Inst = true;
            else
                $scope.is_Prof_Inst = false;
        }, function error(resp) {
            $scope.processing = resp.data.Message;
        });
    }

    $scope.set_displaychatlist = function () {
        $scope.Displaychatlist = !$scope.Displaychatlist;
    }

}]);