﻿icamodule.controller("pendingchat", ['ica_groups_service', '$scope', '$http', 'webAPIurl', '$interval', function (ica_groups_service, $scope, $http, webAPIurl, $interval) {

 //   $scope.orderByField = 'NoOfDays';
  //  $scope.reverseSort = true;

  //  $scope.Displaychatlist = 
    var startint = $interval(function () {
        $scope.waitdisplay = false;
        $scope.icagroups = ica_groups_service.geticagroupslookups();
        if ($scope.icagroups != '')
            $scope.stopint();
    }, 500);

    $scope.stopint = function () {
        //    if ($scope.userlookup != '' && $scope.gendelookup != '' && $scope.statuslookup != '')
        $interval.cancel(startint);
        $scope.waitdisplay = true;
       // console.log($scope.icagroups);
    }
    $scope.pchat_groupId = 0;

    $scope.get_pchat_details = function()
    {
     //   alert($scope.pchat_groupId);
        $scope.processing = 'Processing....'
        $scope.Displaychatlist = false;
        $scope.pchatmessages = '';
        var geticagroups = $http({
            method: "get",
            url: webAPIurl + '/ICAApi/Chat/' + $scope.pchat_groupId,
        //    url: webAPIurl + '/Portal/GetPendingChat/' + $scope.pchat_groupId,
            dataType: 'json',
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json, text/javascript"
            }
        }).then(function (res, status) {
       //     alert('pending chat details ' + JSON.stringify(res.data.Response));
                if (res.data.Response == null || res.data.Response == undefined || res.data.Response == "" || res.data.Response.lenght == 0) {
                 //   alert("No Data found");
                    $scope.processing = 'No Data found'
                    return
                }
                $scope.pchatmessages = res.data.Response;
                $scope.processing = '';
           //     console.log(res.data.Response);
        }, 
         function error(resp) {
             $scope.processing= resp.data.Message;
        });
    }

    $scope.getselectchathistory = function (model)
    {
        model.GroupId = $scope.pchat_groupId;
        $scope.processing = 'Processing....'
        //  alert('angular.toJson(model)');
        var chat_history = $http({
            method: "POST",
            url: webAPIurl + '/ICAApi/Chat',
        //    url: webAPIurl + '/Portal/GetPendingChat/' + $scope.pchat_groupId,
            dataType: 'json',
            data: model,
            //   data: model,
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json, text/javascript"
            }
        });

        chat_history.then(function (resp, status) {
           console.log(resp.data.Response);
           $scope.detailchat = resp.data.Response;
            $scope.processing = '';  // 
            $scope.Displaychatlist = true;
            if ($scope.detailchat.StudentId != 0)
                $scope.is_Prof_Inst = true;
            else
                $scope.is_Prof_Inst = false;
        }, function error(resp) {
            $scope.processing = resp.data.Message;
        });
       // window.location.href = '/Chat/History/senderid =' + model +', receiverid = '+model+', groupid ='+$scope.pchat_groupId;
    }

    $scope.set_displaychatlist = function()
    {
        $scope.Displaychatlist = !$scope.Displaychatlist;
    }

    $scope.pinguser = function()
    {
        alert('Development Pending to ping user')
    }

/*

    var GetpooldetailsbyIdFunction = function () {
        $http({
            method: 'get',
            url: webAPIurl + '/api/SwimPool/GetById/' + $scope.poolid,
            dataType: 'json',
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json, text/javascript"
            }
        }).then(function (res, status) {
            $scope.pool = res.data.Response;
            $scope.pool.StatusId = $scope.pool.StatusId.toString();
            $scope.copyeditmodel = angular.copy($scope.pool);
            console.log(JSON.stringify(res.data.Response));
        },
       function error(resp) {
           alert('Error ' + JSON.stringify(resp.data.Message));
       });
    }

    $scope.insertpool = function () {
        //  alert('angular.toJson(model)');
        var inspool = $http({
            method: "POST",
            url: webAPIurl + '/api/SwimPool/Registration',
            dataType: 'json',
            data: $scope.pool,
            //   data: model,
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json, text/javascript"
            }
        });

        inspool.then(function (resp, status) {
            if (resp.data.Status == 1) {
                alert("Pool Registered Successfully ");
                window.location = '';
                $scope.apply();
            }
            else {
                alert(JSON.stringify(resp.data.Message))
            }
        }, function error(responce) {
            alert('Error ' + JSON.stringify(responce.data.Message));
        });
    }

    switch (angular.element('#formtype').val()) {

        case 'edit':
            {
               


                if (angular.element('#pooldetailsbyid').val() > 0) {
                    $scope.poolid = angular.element('#pooldetailsbyid').val();
                    GetpooldetailsbyIdFunction();
                }

            }

    };

    $scope.poolid = angular.element('#pooldetailsbyid').val();

    $scope.GetpooldetailsbyId = GetpooldetailsbyIdFunction;

    $scope.get_l_details = function () {
        console.log($scope.pool)
    }

    $scope.updatePool = function () {

        var post = $http({
            method: "POST",
            url: webAPIurl + 'api/SwimPool/UpdatePool',
            dataType: 'json',
            data: $scope.pool,
            //      data: model,
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json, text/javascript"
            }
        });


        post.then(function (data, status) {
            if (data.data.Status == 1) {
                alert("Pool Updated Successfully ");
                $scope.copyeditmodel = angular.copy($scope.pool);
                $scope.cancelclick();
                //      window.location = '';
                //       $scope.apply();
            }
            else {
                alert(JSON.stringify(data.data.Message))
                //     $scope.copyeditmodel = angular.copy($scope.pool);
                //       window.location = '';
                //       $scope.apply();
            }


        }, function error(responce) {
            alert('Error ' + JSON.stringify(responce.data.Message));

        });

    };
    */
}]);