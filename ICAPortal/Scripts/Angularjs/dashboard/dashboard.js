﻿icamodule.controller("dashboard", ['$scope','$filter', '$http', 'webAPIurl', function ($scope, $filter, $http, webAPIurl) {

    $scope.displaydashboard = true;
    $scope.getstudentcounts = function () {
        $scope.model = {
            ToDateString: $filter('date')($scope.todate, "dd/MM/yyyy"),
            FromDateString: $filter('date')($scope.fromdate, "dd/MM/yyyy")
        }
        $scope.processing = 'Processing....'
        //  alert('angular.toJson(model)');
        var studentscount = $http({
            method: "POST",
            url: webAPIurl + '/icaapi/GetDashBoardStudents',
            dataType: 'json',
            data: $scope.model,
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json, text/javascript"
            }
        });

        studentscount.then(function (resp, status) {
            console.log(resp.data.Response);
            $scope.students = resp.data.Response;
            $scope.processing = '';  // 
            //$scope.waitdisplay = true;

        }, function error(resp) {
            $scope.processing = resp.data.Message;
        });
        // window.location.href = '/Chat/History/senderid =' + model +', receiverid = '+model+', groupid ='+$scope.select_groupId;
    }


    $scope.waitdisplay = true;

    $scope.todate = new Date();
    $scope.fromdate = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate() - $scope.todate.getDate() + 1);
    //  
    $scope.model = {
        ToDateString: $filter('date')($scope.todate, "dd/MM/yyyy"),
        FromDateString: $filter('date')($scope.fromdate, "dd/MM/yyyy")
    }

    $scope.getstudentcounts();
 
  
       
       $scope.getselect = function (student, rowindex) {
    //     alert(student.group + ' count  ' + student.count + ' Index  ' + rowindex);
           $scope.model = {
             CourseId : rowindex,
             ToDateString: $filter('date')($scope.todate, "dd/MM/yyyy"),
             FromDateString: $filter('date')($scope.fromdate, "dd/MM/yyyy")
         }
         $scope.processing = 'Processing....'
         //  alert('angular.toJson(model)');
         var studentsdetails = $http({
             method: "POST",
             url: webAPIurl + '/icaapi/GetDashBoardCourceStudents',
             dataType: 'json',
             data: $scope.model,
             headers: {
                 "Content-Type": "application/json",
                 "Accept": "application/json, text/javascript"
             }
         });

         studentsdetails.then(function (resp, status) {
             $scope.displaydashboard = false;
             console.log(resp.data.Response);
             $scope.studentsdetailslist = resp.data.Response;
             $scope.processing = '';  // 
             //$scope.waitdisplay = true;

         }, function error(resp) {
             $scope.processing = resp.data.Message;
         });
         // window.location.href = '/Chat/History/senderid =' + model +', receiverid = '+model+', groupid ='+$scope.select_groupId;
       }

       $scope.backbuttonclick = function()
       {
           $scope.displaydashboard = true;
       }
}]);