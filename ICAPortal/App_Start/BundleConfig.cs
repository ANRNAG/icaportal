﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;

namespace ICAPortal
{
    public static class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/dashboard").Include(
              "~/Scripts/Angularjs/angular.js",
              "~/Scripts/Angularjs/icaappconstant.js",
              "~/Scripts/Angularjs/icaservices.js",
              "~/Scripts/Angularjs/dashboard/dashboard.js"));

            bundles.Add(new ScriptBundle("~/bundles/proflist").Include(
              "~/Scripts/Angularjs/angular.js",
              "~/Scripts/Angularjs/icaappconstant.js",
              "~/Scripts/Angularjs/icaservices.js",
              "~/Scripts/Angularjs/list/listprof.js"));

            bundles.Add(new ScriptBundle("~/bundles/universitylist").Include(
              "~/Scripts/Angularjs/angular.js",
              "~/Scripts/Angularjs/icaappconstant.js",
              "~/Scripts/Angularjs/icaservices.js",
              "~/Scripts/Angularjs/list/listuniversity.js"));

            bundles.Add(new ScriptBundle("~/bundles/studentlist").Include(
              "~/Scripts/Angularjs/angular.js",
              "~/Scripts/Angularjs/icaappconstant.js",
              "~/Scripts/Angularjs/icaservices.js",
              "~/Scripts/Angularjs/list/liststudents.js"));
            bundles.Add(new ScriptBundle("~/bundles/lastchat").Include(
               "~/Scripts/Angularjs/angular.js",
               "~/Scripts/Angularjs/icaappconstant.js",
               "~/Scripts/Angularjs/icaservices.js",
               "~/Scripts/Angularjs/chat/lastchat.js"));

            bundles.Add(new ScriptBundle("~/bundles/pendingchat").Include(
                "~/Scripts/Angularjs/angular.js",
                "~/Scripts/Angularjs/icaappconstant.js",
                "~/Scripts/Angularjs/icaservices.js",
                "~/Scripts/Angularjs/chat/pendingchat.js"));

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                "~/Scripts/default/jquery-{version}.js"));
            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/default/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/default/bootstrap.js",
                      "~/Scripts/default/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));
        }
    }
}