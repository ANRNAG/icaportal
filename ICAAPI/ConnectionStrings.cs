﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ICAAPI.Models
{
    public class ConnectionStrings
    {
        public static string[] groups = new string[5] {
            "Chemical",
            "Computers",
            "Civil",
            "Electrical",
            "Mechanical"
        };

        public static string[] connection = new string[6] {
            "Server=tcp:ciapp.database.windows.net,1433;Database=CI_CH;User ID=ci;Password=elite@123;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;",
            "Server=tcp:ciapp.database.windows.net,1433;Database=CI_CS;User ID=ci;Password=elite@123;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;",
            "Server=tcp:ciapp.database.windows.net,1433;Database=CI_CV;User ID=ci;Password=elite@123;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;",
            "Server=tcp:ciapp.database.windows.net,1433;Database=CI_EE;User ID=ci;Password=elite@123;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;",
            "Server=tcp:ciapp.database.windows.net,1433;Database=CI_ME;User ID=ci;Password=elite@123;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;",
            "Server=tcp:ciapp.database.windows.net,1433;Database=CI_Wrapper;User ID=ci;Password=elite@123;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;"
            // 6th connection string to get all student details from wrapper 
        };

        //public static string[] connection = new string[11] {
        //    "Server=tcp:admissions.database.windows.net,1433;Database=ICA_Business_Account;User ID=iac;Password=elite@123;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;",
        //    "Server=tcp:admissions.database.windows.net,1433;Database=ICA_Business_Finance;User ID=iac;Password=elite@123;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;",
        //    "Server=tcp:admissions.database.windows.net,1433;Database=ICA_Business_HR;User ID=iac;Password=elite@123;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;",
        //    "Server=tcp:admissions.database.windows.net,1433;Database=ICA_Business_Marketing;User ID=iac;Password=elite@123;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;",
        //    "Server=tcp:admissions.database.windows.net,1433;Database=ICA_Business_Quality;User ID=iac;Password=elite@123;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;",
        //    "Server=tcp:admissions.database.windows.net,1433;Database=ICA_Engineer_CS;User ID=iac;Password=elite@123;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;",
        //    "Server=tcp:admissions.database.windows.net,1433;Database=ICA_Engineer_EC;User ID=iac;Password=elite@123;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;",
        //    "Server=tcp:admissions.database.windows.net,1433;Database=ICA_Engineer_Embedded;User ID=iac;Password=elite@123;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;",
        //    "Server=tcp:admissions.database.windows.net,1433;Database=ICA_Engineer_IT;User ID=iac;Password=elite@123;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;",
        //    "Server=tcp:admissions.database.windows.net,1433;Database=ICA_Engineer_Mech;User ID=iac;Password=elite@123;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;",
        //    "Server=tcp:admissions.database.windows.net,1433;Database=ICA_Wrapper;User ID=iac;Password=elite@123;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;"
        //};

        //public static string[] connection = new string[11] {
        //    "Server=tcp:admissions.database.windows.net,1433;Database=ICA_Business_Account_Staging;User ID=iac;Password=elite@123;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;",
        //    "Server=tcp:admissions.database.windows.net,1433;Database=ICA_Business_Finance_Staging;User ID=iac;Password=elite@123;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;",
        //    "Server=tcp:admissions.database.windows.net,1433;Database=ICA_Business_HR_Staging;User ID=iac;Password=elite@123;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;",
        //    "Server=tcp:admissions.database.windows.net,1433;Database=ICA_Business_Marketing_Staging;User ID=iac;Password=elite@123;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;",
        //    "Server=tcp:admissions.database.windows.net,1433;Database=ICA_Business_Quality_Staging;User ID=iac;Password=elite@123;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;",
        //    "Server=tcp:admissions.database.windows.net,1433;Database=ICA_Engineer_CS_Staging;User ID=iac;Password=elite@123;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;",
        //    "Server=tcp:admissions.database.windows.net,1433;Database=ICA_Engineer_EC_Staging;User ID=iac;Password=elite@123;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;",
        //    "Server=tcp:admissions.database.windows.net,1433;Database=ICA_Engineer_Embedded_Staging;User ID=iac;Password=elite@123;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;",
        //    "Server=tcp:admissions.database.windows.net,1433;Database=ICA_Engineer_IT_Staging;User ID=iac;Password=elite@123;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;",
        //    "Server=tcp:admissions.database.windows.net,1433;Database=ICA_Engineer_Mech_Staging;User ID=iac;Password=elite@123;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;",
        //    "Server=tcp:admissions.database.windows.net,1433;Database=ICA_Wrapper_Staging;User ID=iac;Password=elite@123;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;"
        //};

       

       
    }
}