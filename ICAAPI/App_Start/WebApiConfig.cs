﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace ICAAPI
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            config.EnableCors();
            
            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "Default",
                routeTemplate: "icaapi/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            // Remove the XML formatter
            config.Formatters.Remove(config.Formatters.XmlFormatter);

            // Add the JSON formatter
            config.Formatters.Add(config.Formatters.JsonFormatter);
            // config.Formatters.XmlFormatter.SupportedMediaTypes.Clear();
        }
    }
}
