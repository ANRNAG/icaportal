﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ICAAPI
{
    public class Utility
    {
        public class CustomResponse
        {
            public CustomResponseStatus Status;
            public Object Response;
            public string Message;
        }
        public enum CustomResponseStatus
        {
            Successful,
            UnSuccessful,
            Exception,
            UnmappedUniversityUser, // Exceptional only for when coach is not mapped to the university
            PaymentInProgress, // Coach has not done the Payment
            CoachNewUser, // Coach does not exist
            AthleteInOtherSport, //Athlete Profile Check
            AtheleteNotAssignedUniversity
        }

        public enum ICASenderType
        {
            Student, // Athelete,0
            Professor,  // Coach,1
            CollegeAthlete, // not exisist
            FamilyFriends,// not exisist
            Recommendor  //ClubCoach, 4
        }

        public class DatesBetween
        {
            public int CourseId { get; set; }
            public string FromDateString { get; set; }
            public string ToDateString { get; set; }
        }
        public class dashboardstudentcount
        {
            public string group { get; set; }
            public int count { get; set; }
        }
    }

   
   
}