﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;

using System.Linq;
using System.Web;

namespace ICAAPI.Models.Chat
{
    public class ICAChatModel
    {
        #region deletedcode
        /*   
            public List<lastchat> getchathistory(lastchat model)
        {
            List<lastchat> listdata = new List<lastchat>();
           
        string cmdtext = "select c.id, c.senderid, c.receiverid, sendertype, receivertype, c.date, c.message from chats c ";
        cmdtext += "where(c.senderid = " + model.Senderid + " and c.receiverid = " + model.Receiverid + ")  or(c.senderid = " + model.Receiverid + " and c.receiverid = " + model.Senderid + ") order by c.id ";

            using (SqlConnection myconn = new SqlConnection(ConfigurationManager.ConnectionStrings["IcaWebApiDB"].ConnectionString))
            {
                myconn.ConnectionString = ConnectionStrings.connection[model.GroupId];

                using (SqlCommand cmd = new SqlCommand(cmdtext, myconn) { CommandType = CommandType.Text })
                {
                    myconn.Open();
                    SqlDataReader SQLData = cmd.ExecuteReader();
                    if (!SQLData.HasRows)
                    {
                        return null;
                    }

                    while (SQLData.Read())
                    {
                        // id senderid receiverid sendertype  receiverType date    message senderid    receiverid lastvisiteddate
                        lastchat singledata = new lastchat();
        singledata.Id = int.Parse(SQLData["id"].ToString());
                        singledata.Senderid = int.Parse(SQLData["senderid"].ToString());
                        singledata.Receiverid = int.Parse(SQLData["receiverid"].ToString());
                        singledata.SenderType = GetChatUserType(int.Parse(SQLData["sendertype"].ToString()));
                        singledata.ReceiverType = GetChatUserType(int.Parse(SQLData["receiverType"].ToString()));
                        singledata.Message_date = (DateTime)SQLData["date"];
                        singledata.Message = SQLData["message"].ToString();
        // singledata.lastvisit = (SQLData["lastvisiteddate"] == DBNull.Value) ? DateTime.Parse("01/01/2000", new CultureInfo("en-CA")) : (DateTime)SQLData["lastvisiteddate"];
        listdata.Add(singledata);
                    }
}
            }

            return listdata;
        }
         *   public class delchathistorydetails
           {
               public int Id { get; set; }
               public int StudendId { get; set; }
               public string StudentName { get; set; }
               public int RecomenderId { get; set; }
               public string RecomenderName { get; set; }
               public int InstituteId { get; set; }
               public string InstituteName { get; set; }
               public string Comment { get; set; }
               public List<lastchat> ChatHistory { get; set; }
           }

      *      public List<chathistorydetails> getrecomenderchatdetails(lastchat model)
           {
               List<chathistorydetails> listdata = new List<chathistorydetails>();

               var senderid = 0; // write code 

               if (model.SenderType == (GetChatUserType(4))) // recommender
                   senderid = model.Senderid;
               else
                   senderid = model.Receiverid;

               string cmdtext = "select rc.id Id, RC.Comment Comment, a.Id StudendId, a.Name StudentName, cc.id RecomenderId, cc.name RecomenderName, ins.Id InstituteId, ins.Name InstituteName from RecommendedInstitutes rc ";
               cmdtext += "inner join Athelete a on rc.AtheleteId = A.id ";
               cmdtext += "inner join ClubCoach cc on rc.clubcoachid = cc.id ";
               cmdtext += "inner join Institution ins on rc.instituteid = ins.Id where rc.id = "+ senderid ;

               using (SqlConnection myconn = new SqlConnection(ConfigurationManager.ConnectionStrings["IcaWebApiDB"].ConnectionString))
               {
                   myconn.ConnectionString = ConnectionStrings.connection[model.GroupId];

                   using (SqlCommand cmd = new SqlCommand(cmdtext, myconn) { CommandType = CommandType.Text })
                   {
                       myconn.Open();
                       SqlDataReader SQLData = cmd.ExecuteReader();
                       if (!SQLData.HasRows)
                       {
                           return null;
                       }

                       while (SQLData.Read())
                       {
                           // id senderid receiverid sendertype  receiverType date    message senderid    receiverid lastvisiteddate
                           chathistorydetails singledata = new chathistorydetails ();
                           singledata.Id = int.Parse(SQLData["Id"].ToString());
                           singledata.StudendId = int.Parse(SQLData["StudendId"].ToString());
                           singledata.StudentName = (SQLData["StudentName"].ToString());
                           singledata.RecomenderId = int.Parse(SQLData["RecomenderId"].ToString());
                           singledata.RecomenderName = (SQLData["RecomenderName"].ToString());
                           singledata.InstituteId = int.Parse(SQLData["InstituteId"].ToString());
                           singledata.InstituteName = (SQLData["InstituteName"].ToString());
                           singledata.Comment = SQLData["Comment"].ToString();
                           //            singledata.lastvisit = (SQLData["lastvisiteddate"] == DBNull.Value) ? DateTime.Parse("01/01/2000", new CultureInfo("en-CA")) : (DateTime)SQLData["lastvisiteddate"];

                           listdata.Add(singledata);
                       }
                   }
               }

               return listdata;
           }
   */
        #endregion

        #region ...private
        private DateTime todayDate = DateTime.Now;
        private SqlCommand sqlcmd = new SqlCommand();
        private SqlConnection sqlcon = new SqlConnection();
        private string GetChatUserType(int usertypeId)
        {
            Utility.ICASenderType enumDisplayStatus = (Utility.ICASenderType)usertypeId;
            return enumDisplayStatus.ToString();
        }
        private chathistorydetails setProfuserDetails(lastchat model, chathistorydetails partial)
        {
            List<chathistorydetails> listdata = new List<chathistorydetails>();

            var senderid = 0; // write code 
            string cmdtext = "";
            cmdtext = "select rc.id Id, RC.Comment Comment, a.Id StudentId, a.Name StudentName, a.Phonenumber StudentPhone, a.Emailaddress StudentEmail, ";

            if (model.SenderType == GetChatUserType(4)) // recommender
            {
                senderid = model.Senderid;
                cmdtext += "cc.id SenderId,'Recommendor' sendertype, cc.name SenderName, cc.Emailaddress SenderEmail, cc.Phonenumber SenderPhone, "; // Recommender
                cmdtext += "ins.Id ReceiverId, 'Professor' receivertype, ins.Name ReceiverName, ins.EmailAddress ReceiverEmail, ins.PhoneNumber ReceiverPhone from RecommendedInstitutes rc "; //Institute
              //  cmdtext += "inner join Athelete a on rc.AtheleteId = A.id ";
              //  cmdtext += "inner join ClubCoach cc on rc.clubcoachid = cc.id ";
              //  cmdtext += "inner join Institution ins on ins.Id = "+ model.Receiverid+" where rc.id = " + senderid;
            }

            else if (model.ReceiverType == GetChatUserType(4)) // recommender
            {
                senderid = model.Receiverid;
                cmdtext += "cc.id ReceiverId,'Recommendor' receivertype, cc.name ReceiverName,  cc.EmailAddress ReceiverEmail, cc.PhoneNumber ReceiverPhone, "; // Recommender
                cmdtext += "ins.Id SenderId, 'Professor' sendertype, ins.Name SenderName, ins.Emailaddress SenderEmail, ins.Phonenumber SenderPhone from RecommendedInstitutes rc "; //Institute
             //   cmdtext += "inner join Athelete a on rc.AtheleteId = A.id ";
            //    cmdtext += "inner join ClubCoach cc on rc.clubcoachid = cc.id ";
            //    cmdtext += "inner join Institution ins on ins.Id = "+ model.Senderid+" where rc.id = " + senderid;
            }

          //  cmdtext = "select rc.id Id, RC.Comment Comment, a.Id StudentId, a.Name StudentName, a.Phonenumber StudentPhone, a.Emailaddress StudentEmail, ";
        //    cmdtext += "cc.id SenderId, cc.name SenderName,  cc.EmailAddress SenderEmail, cc.PhoneNumber SenderPhone, "; // Recommender
       //     cmdtext += "ins.Id ReceiverId, ins.Name ReceiverName, ins.Emailaddress ReceiverEmail, ins.Phonenumber ReceiverPhone from RecommendedInstitutes rc "; //Institute
            cmdtext += "inner join Athelete a on rc.AtheleteId = A.id ";
            cmdtext += "inner join ClubCoach cc on rc.clubcoachid = cc.id ";
            cmdtext += "inner join Institution ins on ins.Id = " + model.Senderid + " where rc.id = " + senderid;



            using (SqlConnection myconn = new SqlConnection(ConfigurationManager.ConnectionStrings["IcaWebApiDB"].ConnectionString))
            {
                myconn.ConnectionString = ConnectionStrings.connection[model.GroupId];

                using (SqlCommand cmd = new SqlCommand(cmdtext, myconn) { CommandType = CommandType.Text })
                {
                    myconn.Open();
                    SqlDataReader SQLData = cmd.ExecuteReader();
                    if (!SQLData.HasRows)
                    {
                        return null;
                    }

                    while (SQLData.Read())
                    {
                        // id senderid receiverid sendertype  receiverType date    message senderid    receiverid lastvisiteddate
                        partial.Id = int.Parse(SQLData["Id"].ToString());
                        partial.StudentId = int.Parse(SQLData["StudentId"].ToString());
                        partial.StudentName = (SQLData["StudentName"].ToString());
                        partial.StudentPhone = (SQLData["StudentPhone"].ToString());
                        partial.StudentEmail = (SQLData["StudentEmail"].ToString());

                        partial.ReceiverId = int.Parse(SQLData["ReceiverId"].ToString());
                        partial.ReceiverType = (SQLData)["receivertype"].ToString();
                        partial.ReceiverName = (SQLData["ReceiverName"].ToString());
                        partial.ReceiverPhone = (SQLData["ReceiverPhone"].ToString());
                        partial.ReceiverEmail = (SQLData["ReceiverEmail"].ToString());

                        partial.SenderId = int.Parse(SQLData["SenderId"].ToString());
                        partial.SenderType = (SQLData)["sendertype"].ToString();
                        partial.SenderName = (SQLData["SenderName"].ToString());
                        partial.SenderPhone = (SQLData["SenderPhone"].ToString());
                        partial.SenderEmail = (SQLData["SenderEmail"].ToString());

                        partial.Comment = SQLData["Comment"].ToString();
                        //            singledata.lastvisit = (SQLData["lastvisiteddate"] == DBNull.Value) ? DateTime.Parse("01/01/2000", new CultureInfo("en-CA")) : (DateTime)SQLData["lastvisiteddate"];
                    }
                }
            }

            return partial;
        }
        private chathistorydetails setUsersDetails(lastchat model, chathistorydetails partial)
        {
            List<chathistorydetails> listdata = new List<chathistorydetails>();

            string cmdtext = "";
            cmdtext = "select c.id Id, '' Comment, 0 StudentId, '' StudentName, '' StudentPhone, '' StudentEmail, ";

            int usertypeval = (int)(Utility.ICASenderType)Enum.Parse(typeof(Utility.ICASenderType), model.SenderType);

            switch (usertypeval)
            { 
                case 0:  // Student, // Athelete,
                    {
                        cmdtext += "a.id senderId, 'Student' sendertype, a.Name SenderName, a.emailaddress senderEmail, a.phonenumber senderphone from chats c ";
                        cmdtext += "inner join athelete a on c.senderid = a.id ";

                        break;
                    }
                case 1:  // Professor,  // Coach,
                    {
                        //   i.id senderId, i.Name SenderName, i.emailaddress senderEmail, i.phonenumber senderphone from chats c
                        //inner join institution I on c.senderid = I.id

                        cmdtext += "ins.Id SenderId, 'Professor' sendertype, ins.Name SenderName, ins.Emailaddress SenderEmail, ins.Phonenumber SenderPhone from chats c "; //Institute
                        cmdtext += "inner join institution ins on c.senderid = ins.id";
                        break;
                    }
                case 2:
                case 3:
                default:
                    // CollegeAthlete, not exisist
                    {

                        cmdtext += "0 SenderId, ' ' sendertype, '*' SenderName, '*' SenderEmail, '*' SenderPhone from chats c "; //Institute
                  //      cmdtext += "inner join athelete a on c.senderid = a.id ";
                        break;
                    }
                
                case 4:  // Recommendor  //ClubCoach,
                    {
                        cmdtext += "cc.id SenderId, 'Recommendor' sendertype, cc.name SenderName, cc.Emailaddress SenderEmail, cc.Phonenumber SenderPhone from chats c "; // Recommender
                        cmdtext += "inner join clubcoach cc on c.senderid = cc.id ";
                        break;
                    }
            }

            string cmdtextReceiver = "";

            usertypeval = (int)(Utility.ICASenderType)Enum.Parse(typeof(Utility.ICASenderType), model.ReceiverType);

            switch (usertypeval)
            {
                case 0:  // Student, // Athelete,
                    {
                        cmdtextReceiver += ", a2.id ReceiverId, 'Student' receivertype, a2.Name ReceiverName, a2.emailaddress ReceiverEmail, a2.phonenumber Receiverphone from chats c ";
                        cmdtextReceiver += "inner join athelete a2 on c.Receiverid = a2.id ";

                        
                            break;
                    }
                case 1:  // Professor,  // Coach,
                    {
                        //   i.id ReceiverId, i.Name ReceiverName, i.emailaddress ReceiverEmail, i.phonenumber Receiverphone from chats c
                        //inner join institution I on c.Receiverid = I.id
                        cmdtextReceiver += ", ins2.Id ReceiverId, 'Professor' receivertype, ins2.Name ReceiverName, ins2.Emailaddress ReceiverEmail, ins2.Phonenumber ReceiverPhone from chats c "; //Institute
                        cmdtextReceiver += "inner join institution ins2 on c.Receiverid = ins2.id";
                        break;
                    }
                case 2:
                case 3:
                default:
                    // CollegeAthlete, not exisist
                    {

                        cmdtextReceiver += ", 0 ReceiverId, ' ' receivertype, ' ' ReceiverName, ' ' ReceiverEmail, ' ' ReceiverPhone from chats c "; //Institute       //        cmdtextReceiver += "inner join athelete a on c.Receiverid = a.id ";
                        break;
                    }

                case 4:  // Recommendor  //ClubCoach,
                    {
                        cmdtextReceiver += ", cc2.id ReceiverId, 'Recommender' receivertype, cc2.name ReceiverName, cc2.Emailaddress ReceiverEmail, cc2.Phonenumber ReceiverPhone from chats c "; // Recommender
                        cmdtextReceiver += "inner join clubcoach cc2 on c.Receiverid = cc2.id ";
                        break;
                    }
            }

            cmdtext = cmdtext.Replace("from chats c", cmdtextReceiver);

            cmdtext += " where c.id = " + model.Id;



            using (SqlConnection myconn = new SqlConnection(ConfigurationManager.ConnectionStrings["IcaWebApiDB"].ConnectionString))
            {
                myconn.ConnectionString = ConnectionStrings.connection[model.GroupId];

                using (SqlCommand cmd = new SqlCommand(cmdtext, myconn) { CommandType = CommandType.Text })
                {
                    myconn.Open();
                    SqlDataReader SQLData = cmd.ExecuteReader();
                    if (!SQLData.HasRows)
                    {
                        return null;
                    }

                    while (SQLData.Read())
                    {
                        // id senderid receiverid sendertype  receiverType date    message senderid    receiverid lastvisiteddate
                        partial.Id = int.Parse(SQLData["Id"].ToString());
                        partial.StudentId = int.Parse(SQLData["StudentId"].ToString());
                        partial.StudentName = (SQLData["StudentName"].ToString());
                        partial.StudentPhone = (SQLData["StudentPhone"].ToString());
                        partial.StudentEmail = (SQLData["StudentEmail"].ToString());

                        partial.ReceiverId = int.Parse(SQLData["ReceiverId"].ToString());
                        partial.ReceiverType = (SQLData["receivertype"].ToString());
                        partial.ReceiverName = (SQLData["ReceiverName"].ToString());
                        partial.ReceiverPhone = (SQLData["ReceiverPhone"].ToString());
                        partial.ReceiverEmail = (SQLData["ReceiverEmail"].ToString());

                        partial.SenderId = int.Parse(SQLData["SenderId"].ToString());
                        partial.SenderType = (SQLData["sendertype"].ToString());
                        partial.SenderName = (SQLData["SenderName"].ToString());
                        partial.SenderPhone = (SQLData["SenderPhone"].ToString());
                        partial.SenderEmail = (SQLData["SenderEmail"].ToString());

                        partial.Comment = SQLData["Comment"].ToString();
                        //            singledata.lastvisit = (SQLData["lastvisiteddate"] == DBNull.Value) ? DateTime.Parse("01/01/2000", new CultureInfo("en-CA")) : (DateTime)SQLData["lastvisiteddate"];
                    }
                }
            }

            return partial;
        }

        #endregion

        #region ...Public 
        public List<lastchat> getlastchatlist(int groupid)
        {
            List<lastchat> listdata = new List<lastchat>();
           


            string cmdtext = "select c.id, c.senderid, c.receiverid, sendertype, receivertype, c.date,  c.message from chats c ";
            cmdtext += "where c.date = (select max(c2.date) from chats c2 where c2.senderid = c.senderid and c2.receiverid = c.receiverid or ";
            cmdtext += "c2.senderid = c.receiverid and c2.receiverid = c.senderid ) order by c.id ";

            using (SqlConnection myconn = new SqlConnection(ConfigurationManager.ConnectionStrings["IcaWebApiDB"].ConnectionString))
            {
                myconn.ConnectionString = ConnectionStrings.connection[groupid];

                using (SqlCommand cmd = new SqlCommand(cmdtext, myconn) { CommandType = CommandType.Text })
                {
                    myconn.Open();
                    SqlDataReader SQLData = cmd.ExecuteReader();
                    if (!SQLData.HasRows)
                    {
                        return null;
                    }

                    while (SQLData.Read())
                    {
                        // id senderid receiverid sendertype  receiverType date    message senderid    receiverid lastvisiteddate
                        lastchat singledata = new lastchat();
                        singledata.Id = int.Parse(SQLData["id"].ToString());
                        singledata.Senderid = int.Parse(SQLData["senderid"].ToString());
                        singledata.Receiverid = int.Parse(SQLData["receiverid"].ToString());
                        singledata.SenderType = GetChatUserType(int.Parse(SQLData["sendertype"].ToString()));
                        singledata.ReceiverType = GetChatUserType(int.Parse(SQLData["receiverType"].ToString()));
                        singledata.Message_date = (DateTime)SQLData["date"];
                        singledata.Message = SQLData["message"].ToString();
                        singledata.NoOfDays = (todayDate - singledata.Message_date).Days;
                        //    singledata.lastvisit = (SQLData["lastvisiteddate"] == DBNull.Value) ? DateTime.Parse("01/01/2000", new CultureInfo("en-CA")) : (DateTime)SQLData["lastvisiteddate"];
                        listdata.Add(singledata);
                    }
                }
            }

            return listdata;
        }
        public List<lastchat> getpendingchatlist(int groupid)
        {
            List<lastchat> listdata = new List<lastchat>();

            string cmdtext = "select c.id, c.senderid, c.receiverid, c.sendertype, c.receiverType, c.date, c.message, clv.senderid, clv.receiverid, clv.lastvisiteddate from chats c ";
            cmdtext += "left join chatslastvisited clv on(c.senderid = clv.receiverid and c.receiverid = clv.senderid and c.date > clv.lastvisiteddate) ";
            cmdtext += "where c.senderid not in (select cd.senderid from ChatsLastVisited cd where cd.receiverid = c.receiverid) or clv.lastvisiteddate is not null ";
            cmdtext += "order by c.id";

            using (SqlConnection myconn = new SqlConnection(ConfigurationManager.ConnectionStrings["IcaWebApiDB"].ConnectionString))
            {
                myconn.ConnectionString = ConnectionStrings.connection[groupid];

                using (SqlCommand cmd = new SqlCommand(cmdtext, myconn) { CommandType = CommandType.Text })
                {
                    myconn.Open();
                    SqlDataReader SQLData = cmd.ExecuteReader();
                    if (!SQLData.HasRows)
                    {
                        return null;
                    }

                    while (SQLData.Read())
                    {
                        // id senderid receiverid sendertype  receiverType date    message senderid    receiverid lastvisiteddate
                        lastchat singledata = new lastchat();
                        singledata.Id = int.Parse(SQLData["id"].ToString());
                        singledata.Senderid = int.Parse(SQLData["senderid"].ToString());
                        singledata.Receiverid = int.Parse(SQLData["receiverid"].ToString());
                        singledata.SenderType = GetChatUserType(int.Parse(SQLData["sendertype"].ToString()));
                        singledata.ReceiverType = GetChatUserType(int.Parse(SQLData["receiverType"].ToString()));
                        singledata.Message_date = (DateTime)SQLData["date"];
                        singledata.Message = SQLData["message"].ToString();
                        singledata.Senderid = int.Parse(SQLData["senderid"].ToString());
                        singledata.Receiverid = int.Parse(SQLData["receiverid"].ToString());
                        singledata.lastvisit = (SQLData["lastvisiteddate"] == DBNull.Value) ? DateTime.Parse("01/01/2000", new CultureInfo("en-CA")) : (DateTime)SQLData["lastvisiteddate"];
                        singledata.NoOfDays = (todayDate - singledata.Message_date).Days;
                        //   singledata.NoOfDays = (todayDate - singledata.Message_date).Days; 
                        //      singledata.NoOfDays = (((SQLData["lastvisiteddate"] == DBNull.Value) ? todayDate  : singledata.lastvisit)  - singledata.Message_date).Days;
                        listdata.Add(singledata);
                    }
                }
            }

            return listdata;
        }
        public chathistorydetails getlatestchathistory(lastchat model)
        {
            chathistorydetails listdata = new chathistorydetails();
            string cmdtext = "select c.id, c.senderid, c.receiverid, sendertype, receivertype, c.date, c.message from chats c ";
            cmdtext += "where(c.senderid = " + model.Senderid + " and c.receiverid = " + model.Receiverid + ")  or(c.senderid = " + model.Receiverid + " and c.receiverid = " + model.Senderid + ") order by c.date";


            using (SqlConnection myconn = new SqlConnection(ConfigurationManager.ConnectionStrings["IcaWebApiDB"].ConnectionString))
            {
                myconn.ConnectionString = ConnectionStrings.connection[model.GroupId];

                using (SqlCommand cmd = new SqlCommand(cmdtext, myconn) { CommandType = CommandType.Text })
                {
                    myconn.Open();
                    SqlDataReader SQLData = cmd.ExecuteReader();
                    if (!SQLData.HasRows)
                    {
                        return null;
                    }

                    listdata.ChatHistory = new List<lastchat>();

                    while (SQLData.Read())
                    {
                        // id senderid receiverid sendertype  receiverType date    message senderid    receiverid lastvisiteddate
                        lastchat  singledata = new lastchat();
                        singledata.Id = int.Parse(SQLData["id"].ToString());
                        singledata.Senderid = int.Parse(SQLData["senderid"].ToString());
                        singledata.Receiverid = int.Parse(SQLData["receiverid"].ToString());
                        singledata.SenderType = GetChatUserType(int.Parse(SQLData["sendertype"].ToString()));
                        singledata.ReceiverType = GetChatUserType(int.Parse(SQLData["receiverType"].ToString()));
                        singledata.Message_date = (DateTime)SQLData["date"];
                        singledata.Message = SQLData["message"].ToString();
                        singledata.NoOfDays = (todayDate - singledata.Message_date).Days;
                        // singledata.lastvisit = (SQLData["lastvisiteddate"] == DBNull.Value) ? DateTime.Parse("01/01/2000", new CultureInfo("en-CA")) : (DateTime)SQLData["lastvisiteddate"];
                        //  listdata.LastChat.Add(new lastchat());
                        listdata.ChatHistory.Add(singledata);
                    }
                }
                    // recommender
            }
            if ((model.SenderType == GetChatUserType(4) && model.ReceiverType == GetChatUserType(1)) ||
                    (model.ReceiverType == GetChatUserType(4) && model.SenderType == GetChatUserType(1)))
                listdata = setProfuserDetails(model, listdata);
            else
                listdata = setUsersDetails(model, listdata);
            return listdata;
        }
        
        #endregion
    }


    #region ...models
    public class lastchat
    {
        public int Id { get; set; }
        public int Senderid { get; set; }
        public int Receiverid { get; set; }
        public string SenderType { get; set; }
        public string ReceiverType { get; set; }
        public DateTime Message_date { get; set; }
        public string Message { get; set; }
        public DateTime lastvisit { get; set; }
        public int GroupId { get; set; }
        public int NoOfDays { get; set; }
    }

    public class chathistorydetails
    {
        public int Id { get; set; }
        public int StudentId { get; set; }
        public string StudentName { get; set; }
        public string StudentPhone { get; set; }
        public string StudentEmail { get; set; }
        public int SenderId { get; set; }
        public string SenderType { get; set; }
        public string SenderName { get; set; }
        public string SenderPhone { get; set; }
        public string SenderEmail { get; set; }
        public int ReceiverId { get; set; }
        public string ReceiverType { get; set; }
        public string ReceiverName { get; set; }
        public string ReceiverPhone { get; set; }
        public string ReceiverEmail { get; set; }
       
        public string Comment { get; set; }
        public List<lastchat> ChatHistory { get; set; }
    }
   

    #endregion
}