﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ICAAPI.Models.List
{
    
    public class selectlist
    {
        public int groupId { get; set; }
        public int Id { get; set; }
    }
    public partial class ICAStudentLIst
    {
        public int Id { get; set; }

        [StringLength(500)]
        public string Username { get; set; }

        [StringLength(100)]
        public string Name { get; set; }

        public string Gender { get; set; }

        public DateTime? CreatedDate { get; set; }

        [StringLength(100)]
        public string Emailaddress { get; set; }

        [StringLength(50)]
        public string Phonenumber { get; set; }
        public string College { get; set; }
        public string University { get; set; }

    }
    public partial class ICAStudent  : ICAStudentLIst
    {
        [StringLength(100)]
        public string School { get; set; }

        [StringLength(1000)]
        public string Description { get; set; }

        public double? GPA { get; set; }

        public int? Social { get; set; }

        public int? SAT { get; set; }

        public int? ACT { get; set; }

        public int? InstituteId { get; set; }

        [StringLength(1000)]
        public string ProfilePicURL { get; set; }

        [StringLength(50)]
        public string DOB { get; set; }

        [StringLength(100)]
        public string Address { get; set; }

        [StringLength(500)]
        public string FacebookId { get; set; }

        [StringLength(1000)]
        public string CoverPicURL { get; set; }

        [StringLength(50)]
        public string Latitude { get; set; }

        [StringLength(50)]
        public string Longitude { get; set; }

        public int? FriendsInApp { get; set; }

        public int? SharesApp { get; set; }

        public int? AverageRating { get; set; }

        public int? AcademicLevel { get; set; }

        public int? YearLevel { get; set; }

        public int? FriendsRate { get; set; }

        public int? ShareRate { get; set; }

        public int? SocialRate { get; set; }

        public int? AcademicRate { get; set; }

        public int? AtheleteRate { get; set; }

        public int? Premium { get; set; }

        [StringLength(20)]
        public string Height { get; set; }

        [StringLength(20)]
        public string Weight { get; set; }

        [StringLength(20)]
        public string WingSpan { get; set; }

        [StringLength(20)]
        public string ShoeSize { get; set; }

        public int? AtheleteType { get; set; }

        public bool? Active { get; set; }

        [StringLength(20)]
        public string HeightType { get; set; }

        [StringLength(20)]
        public string WeightType { get; set; }

        [StringLength(20)]
        public string WingSpanType { get; set; }

        [StringLength(20)]
        public string ShoeSizeType { get; set; }

        [StringLength(20)]
        public string Height2 { get; set; }

        [StringLength(50)]
        public string Major { get; set; }

        [StringLength(100)]
        public string ClubName { get; set; }

        [StringLength(100)]
        public string CoachName { get; set; }

        [StringLength(100)]
        public string CoachEmailId { get; set; }

       

        public int? LoginType { get; set; }

        public int? DeviceId { get; set; }

        public int? ClubId { get; set; }

        public bool? enabled { get; set; }

        public DateTime? updated_date { get; set; }

        public int? updated_by { get; set; }

        [StringLength(20)]
        public string VerticalJump { get; set; }

        [StringLength(20)]
        public string BroadJump { get; set; }

        [StringLength(20)]
        public string State { get; set; }

        [StringLength(20)]
        public string StateId { get; set; }

        public bool? Diamond { get; set; }

        public bool? Disabled { get; set; }

        [StringLength(100)]
        public string SchoolCoachName { get; set; }

        [StringLength(100)]
        public string SchoolCoachEmail { get; set; }

        [StringLength(100)]
        public string SchoolZipCode { get; set; }

        public int? GapyearLevel { get; set; }

        [StringLength(2000)]
        public string GapyearDescription { get; set; }

        [StringLength(100)]
        public string Country { get; set; }

        public double? AP { get; set; }

        public int? CountryId { get; set; }

        public int? AthleteTransition { get; set; }

        [Column("20YardShuttleRun")]
        [StringLength(50)]
        public string C20YardShuttleRun { get; set; }

        [Column("60YardShuttleRun")]
        [StringLength(50)]
        public string C60YardShuttleRun { get; set; }

        [StringLength(50)]
        public string KneelingMedBallToss { get; set; }

        [StringLength(50)]
        public string RotationalMedBallToss { get; set; }

        public int? CityId { get; set; }

        public string CityName { get; set; }

        public int? BasicStateId { get; set; }

        public int? streamTypeId { get; set; }

        [StringLength(50)]
        public string GraduationScore { get; set; }

        public int? DesiredCountryId { get; set; }

 
        [StringLength(50)]
        public string GRE { get; set; }

        [StringLength(50)]
        public string TOEFL { get; set; }

        [StringLength(50)]
        public string IELTS { get; set; }

        [StringLength(50)]
        public string PTE { get; set; }

        [StringLength(50)]
        public string GMAT { get; set; }
    }

    public partial class ICAUniversityList
    {
        //Id	Name	YearOfEstablish	StateName	Cost	AcceptanceRate	Type	TuitionInState	TuitionOutState	TotalStudents
        public int Id { get; set; }

        [StringLength(100)]
        public string Name { get; set; }
        public int? YearOfEstablish { get; set; }
        public int? AcceptanceRate { get; set; }
        
        public string StateName { get; set; }
        public int? Cost { get; set; }
        
        [StringLength(100)]
        public string Type { get; set; }

        [StringLength(100)]
        public string TuitionInState { get; set; }

        [StringLength(100)]
        public string TuitionOutState { get; set; }

        public int? TotalStudents { get; set; }

        //TuitionInState	TuitionOutState	TotalStudents
    }

    public partial class ICAUniversity : ICAUniversityList
    {
      
        [StringLength(2000)]
        public string AreasInterested { get; set; }

        [StringLength(1000)]
        public string Address { get; set; }

        [StringLength(1000)]
        public string Description { get; set; }

        [StringLength(100)]
        public string EmailAddress { get; set; }

        public DateTime? CreatedDate { get; set; }

        [StringLength(500)]
        public string FacebookId { get; set; }

        public int? NCAA { get; set; }

        public int? Conference { get; set; }

        public int? AdmissionStandard { get; set; }

        public decimal? AdmittanceRate { get; set; }
        

        [StringLength(500)]
        public string ClassificationName { get; set; }

        [StringLength(50)]
        public string State { get; set; }

        [StringLength(50)]
        public string City { get; set; }

        [StringLength(20)]
        public string Zip { get; set; }

        [StringLength(200)]
        public string WebsiteURL { get; set; }

        [StringLength(200)]
        public string FaidURL { get; set; }

        [StringLength(200)]
        public string ApplURL { get; set; }

        [StringLength(50)]
        public string Latitude { get; set; }

        [StringLength(50)]
        public string Longitude { get; set; }

        public int? EnrollmentNo { get; set; }

        public int? ApplicationNo { get; set; }

        public int? AdmissionNo { get; set; }

        [StringLength(1000)]
        public string ProfilePicURL { get; set; }

        [StringLength(1000)]
        public string CoverPicURL { get; set; }

        public int? AverageRating { get; set; }
        public int? MensTeam { get; set; }

        public int? WomenTeam { get; set; }

        public decimal? AcceptanceAdmission { get; set; }

        public int? Social { get; set; }

       

        public int? NCAARate { get; set; }

        public int? ConferenceRate { get; set; }

        public int? AdmissionRate { get; set; }

        public int? SocialRate { get; set; }

        public int? AtheleteRate { get; set; }

        [StringLength(20)]
        public string PhoneNumber { get; set; }

        public int? Size { get; set; }

        [StringLength(200)]
        public string FacebookURL { get; set; }

        [StringLength(500)]
        public string AthleticConference { get; set; }

        [StringLength(100)]
        public string EnrollmentFee { get; set; }

        [StringLength(100)]
        public string TuitionFee { get; set; }

        public bool? Scholarship { get; set; }

        public int? enabled { get; set; }

        [StringLength(100)]
        public string ScholarshipMen { get; set; }

        [StringLength(100)]
        public string ScholarshipWomen { get; set; }

        [StringLength(2000)]
        public string VirtualVideoURL { get; set; }

        [StringLength(2000)]
        public string MatchesVirtualVideoURL { get; set; }

        [StringLength(2000)]
        public string VirtualThumbnailURL { get; set; }

        [StringLength(2000)]
        public string MatchesVirtualThumbnailURL { get; set; }

        [StringLength(1000)]
        public string ApplicationDeadline { get; set; }

      

        public int? IsPriority { get; set; }
    }

    public partial class ICAProfList
    {
        public int Id { get; set; }

        [Required]
        [StringLength(100)]
        public string EmailAddress { get; set; }

        public DateTime? CreatedDate { get; set; }

        [StringLength(100)]
        public string InstituteName { get; set; }

        [StringLength(200)]
        public string CollegeName { get; set; }

        public int? Year { get; set; }

        [StringLength(100)]
        public string FirstName { get; set; }

        [StringLength(100)]
        public string LastName { get; set; }

    }
    public partial class ICAProf : ICAProfList
    {
       
        [StringLength(500)]
        public string FacebookId { get; set; }

        public int? InstituteId { get; set; }



        public bool? Payment_Paid { get; set; }

        [StringLength(100)]
        public string Password { get; set; }

        [StringLength(1000)]
        public string activation_code { get; set; }

        public bool? active { get; set; }

        [StringLength(500)]
        public string PaymentToken { get; set; }

        [StringLength(500)]
        public string Amount { get; set; }

        public int? TeamType { get; set; }

        public int? HeadCoach { get; set; }

        [StringLength(500)]
        public string PreferredStrokes { get; set; }

        [StringLength(500)]
        public string AuthToken { get; set; }

        public int? AcademicRate { get; set; }

        public int? DeviceId { get; set; }

        public DateTime? activated_date { get; set; }

        public int? activated_by { get; set; }

        [StringLength(100)]
        public string EncryptPassword { get; set; }

        [StringLength(100)]
        public string CoachName { get; set; }

        [StringLength(10)]
        public string Title { get; set; }

        [StringLength(20)]
        public string Phone1 { get; set; }

        [StringLength(20)]
        public string Phone2 { get; set; }

        [StringLength(4000)]
        public string Accolades { get; set; }

      
        public int? ProfessorType { get; set; }
    }
}