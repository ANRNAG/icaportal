﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using System.Globalization;

namespace ICAAPI.Models.List
{
    public class ICAListCRUD
    {
        #region ...private
        private DateTime todayDate = DateTime.Now;
        private SqlCommand sqlcmd = new SqlCommand();
        private SqlConnection sqlcon = new SqlConnection();
        #endregion

        #region ...Public 
        public List<ICAStudentLIst> getStudentLIst(int groupid)
        {
            List<ICAStudentLIst> listdata = new List<ICAStudentLIst>();

            string cmdtext = "select a.id, a.username, a.name,  a.gender, a.createdDate, a.emailaddress, a.phonenumber, inc.name collegename, uni.name universityname from athelete a ";
            cmdtext += "inner join indiancolleges inc on a.collegeid = inc.id ";
            cmdtext += "inner join indianuniversities uni on a.universityid = uni.id order by a.id desc";
            
            using (SqlConnection myconn = new SqlConnection(ConfigurationManager.ConnectionStrings["IcaWebApiDB"].ConnectionString))
            {
                myconn.ConnectionString = ConnectionStrings.connection[groupid];

                using (SqlCommand cmd = new SqlCommand(cmdtext, myconn) { CommandType = CommandType.Text })
                {
                    myconn.Open();
                    SqlDataReader SQLData = cmd.ExecuteReader();
                    if (!SQLData.HasRows)
                    {
                        return null;
                    }

                    while (SQLData.Read())
                    {
                        // idid	username	name	gender	createdDate	emailaddress	phonenumber	collegename	universityname

                        ICAStudentLIst singledata = new ICAStudentLIst();
                        singledata.Id = int.Parse(SQLData["id"].ToString());
                        singledata.Username = SQLData["username"].ToString();
                        singledata.Name = SQLData["name"].ToString();
                        singledata.Gender = ((int.Parse(SQLData["gender"].ToString()) == 0) ? "Female" : "Male");
                        singledata.CreatedDate = (DateTime)SQLData["createdDate"];

                        singledata.Emailaddress = SQLData["emailaddress"].ToString();
                        singledata.Phonenumber = SQLData["phonenumber"].ToString();
                        
                        singledata.College = SQLData["collegename"].ToString();
                        singledata.University = SQLData["universityname"].ToString();
                        //    singledata.lastvisit = (SQLData["lastvisiteddate"] == DBNull.Value) ? DateTime.Parse("01/01/2000", new CultureInfo("en-CA")) : (DateTime)SQLData["lastvisiteddate"];
                        listdata.Add(singledata);
                    }
                }
            }

            return listdata;
        }
        public ICAStudent getStudentDetails(selectlist model)
        {
            ICAStudent singledata = new ICAStudent();
            string cmdtext = "select a.id, a.username, a.name, a.gender, a.createdDate, a.emailaddress, a.phonenumber, a.ProfilePicURL, ";
            cmdtext += "a.GraduationScore, a.GRE, a.Gmat, a.TOEFL, a.ielts, a.pte, ";
            cmdtext += "sta.statename, coun.name countryname, inc.name collegename, uni.name universityname from athelete a ";
            cmdtext += "inner join indiancolleges inc on a.collegeid = inc.id ";
            cmdtext += "inner join indianuniversities uni on a.universityid = uni.id ";
            cmdtext += "inner join Country coun on a.DesiredCountryId = coun.id ";
            cmdtext += "inner join States sta on a.StateId = sta.id where a.id = " + model.Id;

            using (SqlConnection myconn = new SqlConnection(ConfigurationManager.ConnectionStrings["IcaWebApiDB"].ConnectionString))
            {
                myconn.ConnectionString = ConnectionStrings.connection[model.groupId];

                using (SqlCommand cmd = new SqlCommand(cmdtext, myconn) { CommandType = CommandType.Text })
                {
                    myconn.Open();
                    SqlDataReader SQLData = cmd.ExecuteReader();
                    if (!SQLData.HasRows)
                    {
                        return null;
                    }

                    while (SQLData.Read())
                    {
                        // idid	username	name	gender	createdDate	emailaddress	phonenumber	collegename	universityname
                        singledata.Id = int.Parse(SQLData["id"].ToString());
                        singledata.Username = SQLData["username"].ToString();
                        singledata.Name = SQLData["name"].ToString();
                        singledata.Gender = ((int.Parse(SQLData["gender"].ToString()) == 0) ? "Female" : "Male");
                        singledata.CreatedDate = (DateTime)SQLData["createdDate"];
                        singledata.ProfilePicURL = SQLData["ProfilePicURL"].ToString();

                        singledata.Emailaddress = SQLData["emailaddress"].ToString();
                        singledata.Phonenumber = SQLData["phonenumber"].ToString();

                        singledata.GraduationScore = SQLData["GraduationScore"].ToString();
                        singledata.GRE = SQLData["GRE"].ToString();
                        singledata.GMAT = SQLData["Gmat"].ToString();
                        singledata.TOEFL = SQLData["TOEFL"].ToString();
                        singledata.IELTS = SQLData["ielts"].ToString();
                        singledata.PTE = SQLData["pte"].ToString();
                        singledata.State = SQLData["statename"].ToString();
                        singledata.Country = SQLData["countryname"].ToString();

                        singledata.College = SQLData["collegename"].ToString();
                        singledata.University = SQLData["universityname"].ToString();
                        //    singledata.lastvisit = (SQLData["lastvisiteddate"] == DBNull.Value) ? DateTime.Parse("01/01/2000", new CultureInfo("en-CA")) : (DateTime)SQLData["lastvisiteddate"];
                    }
                }
            }

            return singledata;
        }

        public List<ICAUniversityList> getUniversityList(int UniGroupId)
        {
            List<ICAUniversityList> listdata = new List<ICAUniversityList>();

            string cmdtext = "select top 1000 ins.Id, ins.Name, ins.YearOfEstablish, stat.Name StateName, ins.Cost, ins.AcceptanceRate, ins.Type, ins.TuitionInState, ins.TuitionOutState, ins.TotalStudents from institution ins ";
            cmdtext += "inner join states stat on ins.State = stat.id order by Id desc";

            using (SqlConnection myconn = new SqlConnection(ConfigurationManager.ConnectionStrings["IcaWebApiDB"].ConnectionString))
            {
                myconn.ConnectionString = ConnectionStrings.connection[UniGroupId];

                using (SqlCommand cmd = new SqlCommand(cmdtext, myconn) { CommandType = CommandType.Text })
                {
                    myconn.Open();
                    SqlDataReader SQLData = cmd.ExecuteReader();
                    if (!SQLData.HasRows)
                    {
                        return null;
                    }

                    while (SQLData.Read())
                    {
                        //    //Id	Name	YearOfEstablish	StateName	Cost AcceptanceRate	Type	
                       // TuitionInState	TuitionOutState	TotalStudents

                        ICAUniversityList singledata = new ICAUniversityList();
                        singledata.Id = int.Parse(SQLData["id"].ToString());
                        singledata.Name = SQLData["Name"].ToString();
                        singledata.YearOfEstablish = (SQLData["YearOfEstablish"].ToString() == "" ? 0 : int.Parse(SQLData["YearOfEstablish"].ToString()));
                        //((SQLData["YearOfEstablish"] ?? string.Empty) ? 0: int.Parse(SQLData["YearOfEstablish"].ToString()));

                        singledata.Cost =(SQLData["Cost"].ToString() =="" ? 0: int.Parse(SQLData["Cost"].ToString()));
                        singledata.StateName = SQLData["StateName"].ToString();
                        singledata.AcceptanceRate = (SQLData["AcceptanceRate"].ToString() == "" ? 0 : int.Parse(SQLData["AcceptanceRate"].ToString()));
                        singledata.Type = SQLData["Type"].ToString();

                        singledata.TuitionInState = SQLData["TuitionInState"].ToString();
                        singledata.TuitionOutState = SQLData["TuitionOutState"].ToString();
                        singledata.TotalStudents = (SQLData["TotalStudents"].ToString() == "" ? 0 : int.Parse(SQLData["TotalStudents"].ToString()));

                        listdata.Add(singledata);
                    }
                }
            }

            return listdata;
        }

        public ICAUniversity getUniversityDetails(selectlist model)
        {
            ICAUniversity singledata = new ICAUniversity();
            string cmdtext = "select top 1 ins.Id, ins.Name, ins.YearOfEstablish, stat.Name StateName, ins.Cost, ins.AcceptanceRate, ins.Type, ins.TuitionInState, ins.TuitionOutState, ins.TotalStudents,  ";
            cmdtext += "ins.Address, ins.Description, ins.EmailAddress, ins.FacebookId, ins.City, ins.zip, ins.websiteurl, ins.profilepicUrl, ins.mensTeam, ins.womenTeam, ins.phonenumber, ins.scholarshipMen, ins.scholarshipWomen, ins.ApplicationDeadline ";
            cmdtext += "from institution ins ";
            cmdtext += "inner join states stat on ins.State = stat.id where ins.id = " + model.Id;

            using (SqlConnection myconn = new SqlConnection(ConfigurationManager.ConnectionStrings["IcaWebApiDB"].ConnectionString))
            {
                myconn.ConnectionString = ConnectionStrings.connection[model.groupId];

                using (SqlCommand cmd = new SqlCommand(cmdtext, myconn) { CommandType = CommandType.Text })
                {
                    myconn.Open();
                    SqlDataReader SQLData = cmd.ExecuteReader();
                    if (!SQLData.HasRows)
                    {
                        return null;
                    }

                    while (SQLData.Read())
                    {
                        //Id	Name	YearOfEstablish	StateName	Cost	AcceptanceRate	Type	TuitionInState	TuitionOutState	TotalStudents	Address	Description	EmailAddress	FacebookId	City	zip	websiteurl	profilepicUrl	mensTeam	womenTeam	phonenumber	scholarshipMen	scholarshipWomen	ApplicationDeadline
                        
                        singledata.Id = int.Parse(SQLData["id"].ToString());
                        singledata.Name = SQLData["Name"].ToString();
                        singledata.YearOfEstablish = (SQLData["YearOfEstablish"].ToString() == "" ? 0 : int.Parse(SQLData["YearOfEstablish"].ToString()));
                        //((SQLData["YearOfEstablish"] ?? string.Empty) ? 0: int.Parse(SQLData["YearOfEstablish"].ToString()));

                        singledata.Cost = (SQLData["Cost"].ToString() == "" ? 0 : int.Parse(SQLData["Cost"].ToString()));
                        singledata.StateName = SQLData["StateName"].ToString();
                        singledata.AcceptanceRate = (SQLData["AcceptanceRate"].ToString() == "" ? 0 : int.Parse(SQLData["AcceptanceRate"].ToString()));
                        singledata.Type = SQLData["Type"].ToString();

                        singledata.TuitionInState = SQLData["TuitionInState"].ToString();
                        singledata.TuitionOutState = SQLData["TuitionOutState"].ToString();
                        singledata.TotalStudents = (SQLData["TotalStudents"].ToString() == "" ? 0 : int.Parse(SQLData["TotalStudents"].ToString()));

                        //Address Description EmailAddress FacebookId  City zip websiteurl profilepicUrl 

                        singledata.City = SQLData["City"].ToString();
                        singledata.Address = SQLData["Address"].ToString();
                        singledata.Description = SQLData["Description"].ToString();
                        singledata.EmailAddress = SQLData["EmailAddress"].ToString();
                        singledata.FacebookId = SQLData["FacebookId"].ToString();
                        singledata.Zip = SQLData["zip"].ToString();
                        singledata.WebsiteURL = SQLData["websiteurl"].ToString();
                        singledata.ProfilePicURL = SQLData["profilepicUrl"].ToString();

                        //  mensTeam womenTeam   phonenumber scholarshipMen  scholarshipWomen ApplicationDeadline
                        singledata.MensTeam = (SQLData["mensTeam"].ToString() == "" ? 0 : int.Parse(SQLData["mensTeam"].ToString()));
                        singledata.WomenTeam = (SQLData["womenTeam"].ToString() == "" ? 0 : int.Parse(SQLData["womenTeam"].ToString()));
                        singledata.ScholarshipMen = SQLData["scholarshipMen"].ToString();
                        singledata.ScholarshipWomen = SQLData["scholarshipWomen"].ToString();
                        singledata.PhoneNumber = SQLData["phonenumber"].ToString();
                        singledata.ApplicationDeadline = SQLData["ApplicationDeadline"].ToString();
                    }
                }
            }

            return singledata;
        }

        public List<ICAProfList> getProfLIst(int groupid)
        {
            List<ICAProfList> listdata = new List<ICAProfList>();

            string cmdtext = "select top 1000 co.id, co.EmailAddress, co.CreatedDate, ins.name institute, co.collegeName, co.year, co.firstname, co.lastname  from coaches co ";
            cmdtext += "inner join Institution ins on co.instituteid = ins.id   order by id desc";
            
            using (SqlConnection myconn = new SqlConnection(ConfigurationManager.ConnectionStrings["IcaWebApiDB"].ConnectionString))
            {
                myconn.ConnectionString = ConnectionStrings.connection[groupid];

                using (SqlCommand cmd = new SqlCommand(cmdtext, myconn) { CommandType = CommandType.Text })
                {
                    myconn.Open();
                    SqlDataReader SQLData = cmd.ExecuteReader();
                    if (!SQLData.HasRows)
                    {
                        return null;
                    }

                    while (SQLData.Read())
                    {
                        // id firstname	lastname createdDate	EmailAddress 	institute	collegeName	year	
                        //                         252 admissions @wpunj.edu William Paterson University of new jersey   NULL NULL    NULL NULL

                        ICAProfList singledata = new ICAProfList();
                        singledata.Id = int.Parse(SQLData["id"].ToString());
                        singledata.FirstName  = SQLData["firstname"].ToString();
                        singledata.LastName = SQLData["lastname"].ToString();
                        singledata.CreatedDate = (SQLData["CreatedDate"] == DBNull.Value) ? DateTime.Parse("01/01/2000", new CultureInfo("en-CA")) : (DateTime)SQLData["createdDate"];
                        singledata.EmailAddress = SQLData["EmailAddress"].ToString();
                        singledata.InstituteName = SQLData["institute"].ToString();
                        singledata.CollegeName = SQLData["collegeName"].ToString();
                        singledata.Year = (SQLData["year"].ToString() == "" ? 0 : int.Parse(SQLData["year"].ToString()));

                        listdata.Add(singledata);
                    }
                }
            }

            return listdata;
        }
        public ICAStudent getProfDetails(selectlist model)
        {
            //

            ICAStudent singledata = new ICAStudent();
            //string cmdtext = "select a.id, a.username, a.name, a.gender, a.createdDate, a.emailaddress, a.phonenumber, a.ProfilePicURL, ";
            //cmdtext += "a.GraduationScore, a.GRE, a.Gmat, a.TOEFL, a.ielts, a.pte, ";
            //cmdtext += "sta.statename, coun.name countryname, inc.name collegename, uni.name universityname from athelete a ";
            //cmdtext += "inner join indiancolleges inc on a.collegeid = inc.id ";
            //cmdtext += "inner join indianuniversities uni on a.universityid = uni.id ";
            //cmdtext += "inner join Country coun on a.DesiredCountryId = coun.id ";
            //cmdtext += "inner join States sta on a.BasicStateId = sta.id where a.id = " + model.Id;

            //using (SqlConnection myconn = new SqlConnection(ConfigurationManager.ConnectionStrings["IcaWebApiDB"].ConnectionString))
            //{
            //    myconn.ConnectionString = ConnectionStrings.connection[model.groupId];

            //    using (SqlCommand cmd = new SqlCommand(cmdtext, myconn) { CommandType = CommandType.Text })
            //    {
            //        myconn.Open();
            //        SqlDataReader SQLData = cmd.ExecuteReader();
            //        if (!SQLData.HasRows)
            //        {
            //            return null;
            //        }

            //        while (SQLData.Read())
            //        {
            //            // idid	username	name	gender	createdDate	emailaddress	phonenumber	collegename	universityname
            //            singledata.Id = int.Parse(SQLData["id"].ToString());
            //            singledata.Username = SQLData["username"].ToString();
            //            singledata.Name = SQLData["name"].ToString();
            //            singledata.Gender = ((int.Parse(SQLData["gender"].ToString()) == 0) ? "Female" : "Male");
            //            singledata.CreatedDate = (DateTime)SQLData["createdDate"];
            //            singledata.ProfilePicURL = SQLData["ProfilePicURL"].ToString();

            //            singledata.Emailaddress = SQLData["emailaddress"].ToString();
            //            singledata.Phonenumber = SQLData["phonenumber"].ToString();

            //            singledata.GraduationScore = SQLData["GraduationScore"].ToString();
            //            singledata.GRE = SQLData["GRE"].ToString();
            //            singledata.GMAT = SQLData["Gmat"].ToString();
            //            singledata.TOEFL = SQLData["TOEFL"].ToString();
            //            singledata.IELTS = SQLData["ielts"].ToString();
            //            singledata.PTE = SQLData["pte"].ToString();
            //            singledata.State = SQLData["statename"].ToString();
            //            singledata.Country = SQLData["countryname"].ToString();

            //            singledata.College = SQLData["collegename"].ToString();
            //            singledata.University = SQLData["universityname"].ToString();
            //            //    singledata.lastvisit = (SQLData["lastvisiteddate"] == DBNull.Value) ? DateTime.Parse("01/01/2000", new CultureInfo("en-CA")) : (DateTime)SQLData["lastvisiteddate"];
            //        }
            //    }
            //}

            return singledata;
        }

        #endregion
    }
}