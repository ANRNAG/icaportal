﻿using ICAAPI.Models.List;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace ICAAPI.Models.Dashboard
{
    public class DashBoardModel
    {
        #region ...private
        private DateTime todayDate = DateTime.Now;
        private SqlCommand sqlcmd = new SqlCommand();
        private SqlConnection sqlcon = new SqlConnection();
        #endregion

        public List<Utility.dashboardstudentcount> GetAllStudents(Utility.DatesBetween model )
        {
            List<Utility.dashboardstudentcount> listdata = new List<Utility.dashboardstudentcount>();
            //select * from users where usertype = 'st' and createdDate between convert(datetime, '02/02/2018', 103) AND convert(datetime, '19/02/2018', 103)  order by createdDate desc
            string cmdtext = "select count(*) from athelete  where ";
            cmdtext += "createdDate between convert(datetime, '"+model.FromDateString +"', 103) AND convert(datetime, '"+model.ToDateString +"', 103) ";
           
            using (SqlConnection myconn = new SqlConnection(ConfigurationManager.ConnectionStrings["IcaWebApiDB"].ConnectionString))
            {
                int counti = 0;
                foreach (string connectionstring in ConnectionStrings.connection)
                {
                   myconn.ConnectionString = connectionstring;  // connection string 6 represent wrapper database
                    if (myconn.Database == "CI_Wrapper")
                        cmdtext = "select count(*) from Users where createdDate between convert(datetime, '" + model.FromDateString + "', 103) AND convert(datetime, '" + model.ToDateString + "', 103) ";
                    using (SqlCommand cmd = new SqlCommand(cmdtext, myconn) { CommandType = CommandType.Text })
                    {
                        
                        myconn.Open();
                        Utility.dashboardstudentcount singleobj = new Utility.dashboardstudentcount();
                        if (counti >= 5)
                            singleobj.group = "All";
                        else
                            singleobj.group = ConnectionStrings.groups[counti].ToString();
                        singleobj.count =  (int) cmd.ExecuteScalar();
                        listdata.Add(singleobj);
                    }
                    myconn.Close(); //myconn.Dispose();
                    counti += 1;
                }
            }

            return listdata;
        }

        public List<ICAStudentLIst> getCourceStudentLIst(Utility.DatesBetween model)
        {
            List<ICAStudentLIst> listdata = new List<ICAStudentLIst>();

            

            string cmdtext = "select a.id, a.username, a.name,  a.gender, a.createdDate, a.emailaddress, a.phonenumber, inc.name collegename, uni.name universityname from athelete a ";
            cmdtext += "left join indiancolleges inc on a.collegeid = inc.id ";
            cmdtext += "left join indianuniversities uni on a.universityid = uni.id where a.createdDate between convert(datetime, '" + model.FromDateString + "', 103) AND convert(datetime, '" + model.ToDateString + "', 103) order by a.id desc";
            if (model.CourseId >= 5)
            {
                cmdtext = "select a.id, a.username,'' name, ''gender, a.createdDate, a.emailaddress, ''phonenumber, '' collegename, '' universityname from Users a ";
                cmdtext += "where a.createdDate between convert(datetime, '" + model.FromDateString + "', 103) AND convert(datetime, '" + model.ToDateString + "', 103) order by a.id desc";
            }
                

                using (SqlConnection myconn = new SqlConnection(ConfigurationManager.ConnectionStrings["IcaWebApiDB"].ConnectionString))
            {
                myconn.ConnectionString = ConnectionStrings.connection[model.CourseId];

                using (SqlCommand cmd = new SqlCommand(cmdtext, myconn) { CommandType = CommandType.Text })
                {
                    myconn.Open();
                    SqlDataReader SQLData = cmd.ExecuteReader();
                    if (!SQLData.HasRows)
                    {
                        return null;
                    }

                    while (SQLData.Read())
                    {
                        // idid	username	name	gender	createdDate	emailaddress	phonenumber	collegename	universityname

                        ICAStudentLIst singledata = new ICAStudentLIst();
                        singledata.Id = int.Parse(SQLData["id"].ToString());
                        singledata.Username = SQLData["username"].ToString();
                        singledata.Name = SQLData["name"].ToString();

                        singledata.Gender = (string.IsNullOrEmpty(SQLData["gender"].ToString()) ? "" :(int.Parse(SQLData["gender"].ToString()) == 0) ? "Female" : "Male");

                        singledata.CreatedDate = (DateTime)SQLData["createdDate"];

                        singledata.Emailaddress = SQLData["emailaddress"].ToString();
                        singledata.Phonenumber = SQLData["phonenumber"].ToString();

                        singledata.College = SQLData["collegename"].ToString();
                        singledata.University = SQLData["universityname"].ToString();
                        
                        //singledata.College = String.IsNullOrEmpty(SQLData["collegename"].ToString()) ? "-"  : SQLData["collegename"].ToString();
                        //singledata.University = String.IsNullOrEmpty(SQLData["universityname"].ToString()) ? "-" : SQLData["universityname"].ToString(); SQLData["universityname"].ToString();
                        ////    singledata.lastvisit = (SQLData["lastvisiteddate"] == DBNull.Value) ? DateTime.Parse("01/01/2000", new CultureInfo("en-CA")) : (DateTime)SQLData["lastvisiteddate"];
                        listdata.Add(singledata);
                    }
                }
            }

            return listdata;
        }
       
        /*
        public List<DashboardStudents> GetAllStudents()
        {

            List<DashboardStudents> listdata = new List<DashboardStudents>();

            string cmdtext = "select * from users where usertype = 'st' order by createdDate desc";

            using (SqlConnection myconn = new SqlConnection(ConfigurationManager.ConnectionStrings["IcaWebApiDB"].ConnectionString))
            {
                myconn.ConnectionString = ConnectionStrings.connection[5];  // connection string 6 represent wrapper database

                using (SqlCommand cmd = new SqlCommand(cmdtext, myconn) { CommandType = CommandType.Text })
                {
                    myconn.Open();
                    SqlDataReader SQLData = cmd.ExecuteReader();
                    if (!SQLData.HasRows)
                    {
                        return null;
                    }

                    while (SQLData.Read())
                    {
                        // id senderid receiverid sendertype  receiverType date    message senderid    receiverid lastvisiteddate
                        DashboardStudents singledata = new DashboardStudents();
                        singledata.Id = int.Parse(SQLData["id"].ToString());
                        singledata.Emailaddress = SQLData["Emailaddress"].ToString();
                        singledata.Username = SQLData["Username"].ToString();
                        singledata.CreatedDate = (DateTime)SQLData["CreatedDate"];
                        //    singledata.lastvisit = (SQLData["lastvisiteddate"] == DBNull.Value) ? DateTime.Parse("01/01/2000", new CultureInfo("en-CA")) : (DateTime)SQLData["lastvisiteddate"];
                        listdata.Add(singledata);
                    }
                }
            }

            return listdata;
        }
        */
    }
    

    public class DashboardStudents
    {
        public int Id { get; set; }

        [StringLength(100)]
        public string Emailaddress { get; set; }

        [StringLength(500)]
        public string Username { get; set; }

        public int? LoginType { get; set; }

        public DateTime? CreatedDate { get; set; }

        [StringLength(500)]
        public string UNIQID { get; set; }

        [StringLength(20)]
        public string UserType { get; set; }

        public int? DeviceId { get; set; }
    }
        
}
