﻿using ICAAPI.Models.Dashboard;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace ICAAPI.Controllers.Dashboard
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class DashController : ApiController
    {
        readonly Utility.CustomResponse _result = new Utility.CustomResponse();

        [HttpPost] // chat details where the receiver not seen 
        [Route ("icaapi/GetDashBoardStudents")]
        public Utility.CustomResponse GetAllStudents(Utility.DatesBetween model)
        {
            try
            {
           //  string startdate = " "; string enddate = " ";
                DashBoardModel icalist = new DashBoardModel();
                _result.Response = icalist.GetAllStudents(model);
                _result.Message = "Successful";
                _result.Status = Utility.CustomResponseStatus.Successful;
                return _result;
            }
            catch (Exception ex)
            {
                _result.Message = "Error: Internal Exception  " + ex.Message;
                _result.Response = null;
                _result.Status = Utility.CustomResponseStatus.Exception;
                return _result;
            }
        }

        [HttpPost] // chat details where the receiver not seen 
        [Route("icaapi/GetDashBoardCourceStudents")]
        public Utility.CustomResponse GetCourceStudentLIst(Utility.DatesBetween model)
        {
            try
            {
                //  string startdate = " "; string enddate = " ";
                DashBoardModel icalist = new DashBoardModel();
                _result.Response = icalist.getCourceStudentLIst(model);
                _result.Message = "Successful";
                _result.Status = Utility.CustomResponseStatus.Successful;
                return _result;
            }
            catch (Exception ex)
            {
                _result.Message = "Error: Internal Exception  " + ex.Message;
                _result.Response = null;
                _result.Status = Utility.CustomResponseStatus.Exception;
                return _result;
            }
        }
    }
}