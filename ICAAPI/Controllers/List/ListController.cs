﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using ICAAPI.Models.List;

namespace ICAAPI.Controllers.List
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]

    public class ListController : ApiController
    {
        readonly Utility.CustomResponse _result = new Utility.CustomResponse();

        //    public string WrapperApi = ConfigurationManager.ConnectionStrings[1].ToString(); //.AppSettings.AllKeys;

        [HttpGet] // chat details where the receiver not seen 
        public Utility.CustomResponse GetStudentsList(int StuGroupId)
        {
            try
            {
                ICAListCRUD icalist = new ICAListCRUD();
                _result.Response = icalist.getStudentLIst(StuGroupId);
                _result.Message = "Successful";
                _result.Status = Utility.CustomResponseStatus.Successful;
                return _result;
            }
            catch (Exception ex)
            {
                _result.Message = "Error: Internal Exception  " + ex.Message;
                _result.Response = null;
                _result.Status = Utility.CustomResponseStatus.Exception;
                return _result;
            }
        }

        [HttpPost]
        [Route("StudentDetails")]
        public Utility.CustomResponse StudentDetails(selectlist model)
        {
            try
            {
                ICAListCRUD icalist = new ICAListCRUD();
                _result.Response = icalist.getStudentDetails(model);
                _result.Message = "Successful";
                _result.Status = Utility.CustomResponseStatus.Successful;
                return _result;
            }
            catch (Exception ex)
            {
                _result.Message = "Error: Internal Exception  " + ex.Message;
                _result.Response = null;
                _result.Status = Utility.CustomResponseStatus.Exception;
                return _result;
            }
        }


        [HttpGet] // chat details where the receiver not seen 
        public Utility.CustomResponse GetUniversityList(int UniGroupId)
        {
            try
            {
                ICAListCRUD icalist = new ICAListCRUD();
                _result.Response = icalist.getUniversityList(UniGroupId); 
                _result.Message = "Successful";
                _result.Status = Utility.CustomResponseStatus.Successful;
                return _result;
            }
            catch (Exception ex)
            {
                _result.Message = "Error: Internal Exception  " + ex.Message;
                _result.Response = null;
                _result.Status = Utility.CustomResponseStatus.Exception;
                return _result;
            }
        }

        [HttpPost]
        [Route("UniversityDetails")]
        public Utility.CustomResponse UniversityDetails(selectlist model)
        {
            try
            {
                ICAListCRUD icalist = new ICAListCRUD();
                _result.Response = icalist.getUniversityDetails(model);
                _result.Message = "Successful";
                _result.Status = Utility.CustomResponseStatus.Successful;
                return _result;
            }
            catch (Exception ex)
            {
                _result.Message = "Error: Internal Exception  " + ex.Message;
                _result.Response = null;
                _result.Status = Utility.CustomResponseStatus.Exception;
                return _result;
            }
        }

        //select top 1000 co.id, co.EmailAddress, co.InstituteId, co.collegeName, co.year, co.firstname, co.lastname from coaches co

        [HttpGet] // chat details where the receiver not seen 
        public Utility.CustomResponse GetProfList(int ProfGroupId)
        {
            try
            {
                ICAListCRUD icalist = new ICAListCRUD();
                _result.Response = icalist.getProfLIst(ProfGroupId);
                _result.Message = "Successful";
                _result.Status = Utility.CustomResponseStatus.Successful;
                return _result;
            }
            catch (Exception ex)
            {
                _result.Message = "Error: Internal Exception  " + ex.Message;
                _result.Response = null;
                _result.Status = Utility.CustomResponseStatus.Exception;
                return _result;
            }
        }

        [HttpPost]
        [Route("ProfDetails")]
        public Utility.CustomResponse ProfDetails(selectlist model)
        {
            try
            {
                ICAListCRUD icalist = new ICAListCRUD();
                _result.Response = icalist.getProfDetails(model);
                _result.Message = "Successful";
                _result.Status = Utility.CustomResponseStatus.Successful;
                return _result;
            }
            catch (Exception ex)
            {
                _result.Message = "Error: Internal Exception  " + ex.Message;
                _result.Response = null;
                _result.Status = Utility.CustomResponseStatus.Exception;
                return _result;
            }
        }

    }
}
