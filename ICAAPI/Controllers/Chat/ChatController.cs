﻿using ICAAPI.Models.Chat;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace ICAAPI.Controllers.Chat
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ChatController : ApiController
    {
        readonly Utility.CustomResponse _result = new Utility.CustomResponse();

        //    public string WrapperApi = ConfigurationManager.ConnectionStrings[1].ToString(); //.AppSettings.AllKeys;

        [HttpGet] // chat details where the receiver not seen 
        public Utility.CustomResponse GetPendingChat(int id)
        {
            try
            {
                ICAChatModel icachat = new ICAChatModel();
                _result.Response = icachat.getpendingchatlist(id);
                _result.Message = "Successful";
                _result.Status = Utility.CustomResponseStatus.Successful;
                return _result;
            }
            catch (Exception ex)
            {
                _result.Message = "Error: Internal Exception  " + ex.Message;
                _result.Response = null;
                _result.Status = Utility.CustomResponseStatus.Exception;
                return _result;
            }
        }

        [HttpGet] // last chat between sender and receiver it display both seen and unseen last chat message
        public Utility.CustomResponse GetLastChat(int lastchatgroupid)
        {
            try
            {
                ICAChatModel icachat = new ICAChatModel();
                _result.Response = icachat.getlastchatlist(lastchatgroupid);
                _result.Message = "Successful";
                _result.Status = Utility.CustomResponseStatus.Successful;
                return _result;
            }
            catch (Exception ex)
            {
                _result.Message = "Error: Internal Exception  " + ex.Message;
                _result.Response = null;
                _result.Status = Utility.CustomResponseStatus.Exception;
                return _result;
            }
        }

        [HttpPost]  // chat details between sender and receiver
        public Utility.CustomResponse GetChatHistory(lastchat model)
        {
            try
            {
                ICAChatModel icachat = new Models.Chat.ICAChatModel();
                //_result.Response = icachat.getchathistory(model);
                _result.Response = icachat.getlatestchathistory(model);
                _result.Message = "Successful";
                _result.Status = Utility.CustomResponseStatus.Successful;
                return _result;
            }
            catch (Exception ex)
            {
                _result.Message = "Error: Internal Exception  " + ex.Message;
                _result.Response = null;
                _result.Status = Utility.CustomResponseStatus.Exception;
                return _result;
            }
        }

        /*

        [Route("RecomenderChat")]

        [HttpPost]
        public Utility.CustomResponse GetRecomenderChatDetails(lastchat model)
        {
            try
            {
                ICAChatModel icachat = new Models.Chat.ICAChatModel();
                _result.Response = icachat.getrecomenderchatdetails(model);
                _result.Message = "Successful";
                _result.Status = Utility.CustomResponseStatus.Successful;
                return _result;
            }
            catch (Exception ex)
            {
                _result.Message = "Error: Internal Exception  " + ex.Message;
                _result.Response = null;
                _result.Status = Utility.CustomResponseStatus.Exception;
                return _result;
            }
        }
        */
    }
}