﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace ICAAPI.Controllers.Groups
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class GroupsController : ApiController
    {
        readonly Utility.CustomResponse _result = new Utility.CustomResponse();
        public Utility.CustomResponse GetAllGroups()
        {
            try
            {
                _result.Response = ICAAPI.Models.ConnectionStrings.groups; 
                _result.Message = "Successful";
                _result.Status = Utility.CustomResponseStatus.Successful;
                return _result;
            }
            catch (Exception ex)
            {
                _result.Message = "Error: Internal Exception  " + ex.Message;
                _result.Response = null;
                _result.Status = Utility.CustomResponseStatus.Exception;
                return _result;
            }
        }
    }
}